<img src="image/icare.png" alt="icare"/>



# ICARE RELEASE

Copyright &copy; 2014-2024, LAPP/CNRS

Fatih Bellachia [Contact](mailto:&#102;&#097;&#116;&#105;&#104;&#046;&#098;&#101;&#108;&#108;&#097;&#099;&#104;&#105;&#097;&#064;&#108;&#097;&#112;&#112;&#046;&#105;&#110;&#050;&#112;&#051;&#046;&#102;&#114;)



## SYSTEM REQUIREMENTS

  - Scientific Linux 6 (SLC6)[^1] or CentOS 7[^1] or AlmaLinux 9
  - CMT version v1r22
    - http://www.cmtsite.net
  - OpenOCD version 0.9.0 or upper
    - http://sourceforge.net/projects/openocd/files/openocd, source files
    - http://www.freddiechopin.info, Windows binaries
  - GNU ARM embedded toolchain (gcc 9.3.1) :
    - [gcc-arm-none-eabi-9-2020-q2-update](https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2)
    - [newlib patches (debug mode + unwind tables)](https://gitlab.cern.ch/atlas-lar-icare/ICARE/-/raw/ICARE-00-05-00/uploads/newlib_debug_thumb_v7e-m+fp_hard.tar.bz2?inline=false)


[^1]: Deprecated, can still be used to build binaries.


## INSTALLATION & BUILD

  - Only ICARE S/W release
    
      ```bash
      % cd <installation area>
      % ICARE-xx-yy-zz.sh
      % cd ICARE/releases/ICARE-xx-yy-zz
      ```

  - ICARE software stack (i.e. full)

    :information_source: *Available on demand from January 2020*
    
      ```bash
      % cd <installation area>
      % ICARE_DDMMYY.sh
      % cd ICARE/releases/ICARE-xx-yy-zz 
      ```
    
  - GNU ARM embedded toolchain 

    :information_source: *starting at ICARE-00-05-00*
    
      ```bash
      % cd <installation area>/ICARE/contrib
      % mkdir -p gcc/9.3.1/x86_64
      % tar -jxf gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2 --strip-components=1 -C gcc/9.3.1/x86_64
      % tar -jxf newlib_debug_thumb_v7e-m+fp_hard.tar.bz2
      % cd gcc/9.3.1/x86_64/arm-none-eabi/lib/thumb/v7e-m+fp
      % mv hard hard.org
      % ln -s hard.debug hard
      ```

  - Updating user project area against the new version of ICARE

    1. Download your project from a source-code repository and go there

       1.1 Minimal directory structure of project
    
       ```markdown
       <project area> ┐
                      ├── cmt
                      ├── config
                      └── <board>
       ```

    2. Edit and update the cmt/project.cmt file to use the new version of ICARE
    
       ```vim
       use releases ICARE-xx-yy-zz
       ```
    
    3. Remove all local ICARE patches (i.e. ICARE packages)
    
    4. For previous versions of ICARE used which are lower than ICARE-00-03-02
    
       4.1. Download and install the new file [ipconfig.h](https://gitlab.cern.ch/atlas-lar-icare/config/raw/master/config/ipconfig.h?inline=false) in the config/config directory
    
    5. Create the RELEASE file which contains the IPM Controller firmware version
    
       5.1 Version format
    
          ```markdown
          <major>.<minor>.<patch>
       
          major : [0 .. 127]
          minor : [0 .. 9]
          patch : [0 .. 9]
          ```
    
    6. Configure and build your project
    
      ```bash
      % source <installation area>/ICARE/releases/<ICARE-xx-yy-zz>/admin/cmt/setup.sh
      % cd <board>/cmt
      % cmt broadcast -local cmt config
      % make rebuild
      ```

### :mag: *for more information see [doc/README](https://gitlab.cern.ch/atlas-lar-icare/doc/blob/0.6.1/README) file*
